{{- if not .Values.mongoHosts }}
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: {{ include "unifi-network-application.fullname" . }}-mongo
  labels:
    {{- include "unifi-network-application.labels" . | nindent 4 }}
    app.kubernetes.io/component: database
spec:
  replicas: 1
  selector:
    matchLabels:
      {{- include "unifi-network-application.selectorLabels" . | nindent 6 }}
      app.kubernetes.io/component: database
  serviceName: {{ include "unifi-network-application.fullname" . }}-mongo
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "unifi-network-application.selectorLabels" . | nindent 8 }}
        app.kubernetes.io/component: database
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "unifi-network-application.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      terminationGracePeriodSeconds: {{ .Values.mongo.terminationGracePeriodSeconds }}
      volumes:
        {{- if not .Values.mongo.persistentVolume.enabled }}
        - name: data
          emptyDir: {}
        {{- end }}
        - name: admin-credentials
          secret:
            secretName: {{ .Values.mongo.adminUser.existingSecret | quote }}
        - name: unifi-credentials
          secret:
            secretName: {{ .Values.mongoUser.existingSecret | quote }}
        - name: initdb
          configMap:
            name: {{ include "unifi-network-application.fullname" . }}-mongo-initdb
      initContainers:
        - name: data-dirs
          image: "{{ .Values.mongo.image.repository }}:{{ .Values.mongo.image.tag }}"
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          securityContext:
            runAsUser: 0
          volumeMounts:
            - name: data
              mountPath: /data
          command:
            - "sh"
            - "-euxc"
            - |
              install -d -o mongodb -g mongodb -m 0700 /data/configdb
              install -d -o mongodb -g mongodb -m 0700 /data/db
      containers:
        - name: {{ .Chart.Name }}-mongo
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.mongo.image.repository }}:{{ .Values.mongo.image.tag }}"
          imagePullPolicy: {{ .Values.mongo.image.pullPolicy }}
          volumeMounts:
            - name: data
              mountPath: /data/configdb
              subPath: configdb
            - name: data
              mountPath: /data/db
              subPath: db
            - name: admin-credentials
              mountPath: /etc/mongo-credentials/admin
              readOnly: true
            - name: unifi-credentials
              mountPath: /etc/mongo-credentials/unifi
              readOnly: true
            - name: initdb
              mountPath: /docker-entrypoint-initdb.d
              readOnly: true
          env:
            - name: MONGO_INITDB_ROOT_USERNAME_FILE
              value: /etc/mongo-credentials/admin/{{ .Values.mongo.adminUser.existingSecretUsernameKey }}
            - name: MONGO_INITDB_ROOT_PASSWORD_FILE
              value: /etc/mongo-credentials/admin/{{ .Values.mongo.adminUser.existingSecretPasswordKey }}
            - name: MONGO_INITDB_DATABASE
              value: {{ .Values.mongoAuthDb | quote }}
            - name: UNIFI_MONGO_USERNAME_FILE
              value: /etc/mongo-credentials/unifi/{{ .Values.mongoUser.existingSecretUsernameKey }}
            - name: UNIFI_MONGO_PASSWORD_FILE
              value: /etc/mongo-credentials/unifi/{{ .Values.mongoUser.existingSecretPasswordKey }}
            - name: UNIFI_MONGO_DB_NAME
              value: {{ .Values.mongoDbName | quote }}
          ports:
            - name: http
              containerPort: 27017
              protocol: TCP
          livenessProbe:
            exec:
              command:
                - 'mongo'
                - '--eval'
                - 'db.runCommand("ping").ok'
                - 'localhost:27017/test'
            initialDelaySeconds: 30
          readinessProbe:
            exec:
              command:
                - 'mongo'
                - '--eval'
                - 'db.runCommand("ping").ok'
                - 'localhost:27017/test'
            initialDelaySeconds: 5
          resources:
            {{- toYaml .Values.resources | nindent 12 }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
  {{- if .Values.persistentVolume.enabled }}
  volumeClaimTemplates:
    - metadata:
        name: data
        labels:
          {{- include "unifi-network-application.labels" . | nindent 10 }}
          app.kubernetes.io/component: database
        {{- with .Values.persistentVolume.annotations }}
        annotations:
          {{- toYaml . | nindent 10 }}
        {{- end }}
      spec:
        accessModes:
          {{- range .Values.persistentVolume.accessModes }}
          - {{ . | quote }}
          {{- end }}
        resources:
          requests:
            storage: {{ .Values.persistentVolume.size | quote }}
        {{- if kindIs "string" .Values.persistentVolume.storageClassName }}
        storageClassName: {{ .Values.persistentVolume.storageClassName | quote }}
        {{- end }}
        {{- with .Values.persistentVolume.selector }}
        selector:
          {{- toYaml . | nindent 10 }}
        {{- end }}
  {{- end }}
{{- end }}
