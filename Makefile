SHELL := bash

VERSION = $(shell yq -e e .version Chart.yaml)

GPG_SECRING = ~/.gnupg/secring.gpg

GITLAB_USERNAME = $(error Please set GITLAB_USERNAME)
GITLAB_TOKEN = $(error Please set GITLAB_TOKEN)

.PHONY: test lint package clean docs check-readme

lint:
	helm lint ./

test: lint check-readme

dist:
	mkdir -p dist

README.md: values.yaml
	helm-docs

docs: README.md

check-readme:
	@if ! diff --color -du README.md <(helm-docs --dry-run); then printf 'README.md is not up to date!\n' >&2; exit 1; fi

ifeq ($(GPGKEY),)
package: dist
	helm package ./ --destination dist
else
package: dist
	helm package ./ --destination dist --sign --key $(GPG_KEY) --keyring $(GPG_SECRING)
	helm verify dist/unifi-network-application-$(VERSION).tgz
endif

publish:
	curl --request POST --user $(GITLAB_USERNAME):$(GITLAB_TOKEN) --form chart=@dist/unifi-network-application-$(VERSION).tgz https://gitlab.com/api/v4/projects/35607564/packages/helm/api/stable/charts

clean:
	$(RM) -r dist
